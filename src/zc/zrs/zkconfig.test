Configuration
=============

To use ZooKeeper in practice, you'll need to modify your
configuration.  In most cases, that's as simple as adding zookeeper
option and specifying paths in ``replicate-to`` and ``replicate-from``.

    >>> import ZODB.config
    >>> storage = ZODB.config.storageFromString("""
    ... %import zc.zrs
    ... <zrs>
    ...     zookeeper zookeeper.example.com:2181
    ...     replicate-to /to
    ...     replicate-from /from
    ...     keep-alive-delay 42
    ...     <filestorage>
    ...        path data.fs
    ...     </filestorage>
    ... </zrs>
    ... """)

Here we essentially created a primary and a secondary.

    >>> import zc.zrs.zk, ZODB.FileStorage
    >>> zc.zrs.zk.Primary.assert_called_with(
    ...     ZODB.FileStorage.FileStorage.return_value, ('', 0),
    ...     'zookeeper.example.com:2181', '/to')
    >>> zc.zrs.zk.Secondary.assert_called_with(
    ...     zc.zrs.zk.Primary.return_value,
    ...     'zookeeper.example.com:2181', '/from',
    ...     keep_alive_delay=42)

Sometimes, you need to use a different zookeeper for the
replicate-from address than the replication-to address. You might do
this, for example, when replicating accross a WAN.  If you specify a
replicate-from-zookeeper, then it will be used rather than the
zookeeper option:


    >>> import ZODB.config
    >>> storage = ZODB.config.storageFromString("""
    ... %import zc.zrs
    ... <zrs>
    ...     zookeeper zookeeper.example.com:2181
    ...     replicate-from-zookeeper zk.example.com:2181
    ...     replicate-to /to
    ...     replicate-from /from
    ...     keep-alive-delay 42
    ...     <filestorage>
    ...        path data.fs
    ...     </filestorage>
    ... </zrs>
    ... """)

    >>> import zc.zrs.zk, ZODB.FileStorage
    >>> zc.zrs.zk.Primary.assert_called_with(
    ...     ZODB.FileStorage.FileStorage.return_value, ('', 0),
    ...     'zookeeper.example.com:2181', '/to')
    >>> zc.zrs.zk.Secondary.assert_called_with(
    ...     zc.zrs.zk.Primary.return_value,
    ...     'zk.example.com:2181', '/from',
    ...     keep_alive_delay=42)


